<?php

namespace App\Example;

/**
 * Ici, notre classe FirstImplementation implémente l'interface FirstInterface.
 * Cela signifie qu'elle doit  implémenter toutes ses méthodes (ici juste
 * la méthode doStuff avec un paramètre de type int et qui renvoie du string)
 */
class FirstImplementation implements FirstInterface {

    /**
     * Voilà l'implémentation de la méthode doStuff, on doit respecter exactement
     * la définition telle qu'elle est dans l'interface mais après la façon dont
     * on fait le code à l'intérieur est totalement libre.
     * Il pourra y avoir plusieurs implémentations d'une même interface qui n'auront
     * rien à voir à part leur définition
     */
    public function doStuff(int $param): string
    {
        return "bloup" . $param;
    }

}


// function maFonction(FirstInterface $impl) {
//     echo $impl->doStuff(1);
// }

// maFonction(new FirstImplementation());