<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\Persistence\ObjectManager;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/profile", name="profile")
     */
    public function profile(){

        return $this->render('home/profile.html.twig', [
            
        ]);
    }
    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $auth){
        
         // On récupère la dernière erreur d'authentification s'il yen a
        $error = $auth->getLastAuthenticationError();
        //On récupère le dernier identifiant utilisé pour une connexion
        $username = $auth->getLastUsername();

        //On les expose au template qui contient le formulaire de login
        return $this->render('home/login.html.twig', [
            'error' => $error,
            'username' => $username
        ]);
    }
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request,
                            UserPasswordEncoderInterface $encoder, 
                            ObjectManager $manager) {

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            /**
             * Lorsqu'on enregistre un user, il faut encoder son mot de
             * passe en utilisant le UserPasswordEncoderInterface
             */
            $pass = $encoder->encodePassword($user, $user->getPassword());
            //Puis remplacer son mot de passe en clair par le mot de passe hashé
            $user->setPassword($pass);
            
            $manager->persist($user);
            $manager->flush();
        
            return $this->redirectToRoute('login');
        }

        return $this->render('home/register.html.twig', [
            'form' => $form->createView()    
        
        ]);
    }
}
