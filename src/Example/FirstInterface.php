<?php

namespace App\Example;


/**
 * Une interface est un type de "classe" un peu particulière qui ne contiendra
 * que des méthodes abstraites et qui ne pourra pas être instanciée.
 * Pour utiliser une interface, il faudra l'implémenter sur une autre classe.
 * (c'est à dire définir toutes ses méthodes abstraites dans la classe en question)
 * C'est une partie importante de la conception de la POO qui permettra de "forcer"
 * les dév' à coder une classe avec certaines méthodes pour pouvoir l'utiliser
 * ailleurs avec le Polymorphisme (voir le Principe de Substitution de Liskov du SOLID)
 * L'intérêt des interfaces comparé à de l'héritage est qu'une même classe peut
 * implémenter plusieurs interfaces et qu'elles sont moins contraignante que l'héritage
 */
interface FirstInterface {
    /**
     * Une méthode abstraite est juste une "définition de méthode", c'est 
     * à dire le nom de la méthode, ses paramètres et leur type, et son
     * type de retour s'il y a. La méthode abstraite n'a pas de "corps",
     * elle n'a pas le code de la méthode en question, ça sera le rôle de
     * l'implémentation de créer ce code.
     */
    function doStuff(int $param):string;
}